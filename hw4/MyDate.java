public class MyDate {
	private int gun, ay, yil;

		public MyDate (int g, int a, int y)
		{
				if (isValid(g,a,y))
		{
					gun = g;
					ay = a;
					yil = y;}	
				else{
					gun = 0;
					ay = 0;
					yil = 0;
		}
		}

		public int getDay()
		{
			return gun;
		}

		public int getMonth()
		{
			return ay;
		}

		public int getYear()
		{
			return yil;
		}

		public void setDay(int g)
		{
			gun = g;
		}

		public void setMonth(int a)
		{
			ay = a;

		}

		public void setYear(int y)
		{
			yil = y;

		}

		public void setDate(int g, int a, int y)
		{
			if (isValid(g,a,y))
			{
				setMonth(a);
				setDay(g);
				setYear(y);
			}
		}

		public String toString(){
			return yil+"-"+ay+"-"+gun;
		}
		public static boolean isValid (int g, int a, int y)
		 {
		 			if (y > 1900 && a <= 12 && g > 0){
		 					if(a == 1 && g <= 31){
		 					return true;
		 					}else
		 					if(a == 2 && g <= 29 && y % 4 == 0 && y % 400 != 0){
		 					return true;
		 					}else
		 					if(a == 2 && g <= 28 && y % 4 != 0){
		 					return true;
		 					}else
		 					if(a == 3 && g <= 31){
		 					return true;
		 					}else
		 					if(a == 4 && g <= 30){
		 					return true;
		 					}else
		 					if(a == 5 && g <= 31){
		 					return true;
		 					}else
		 					if(a == 6 && g <= 30){
		 					return true;
		 					}else
		 					if(a == 7 && g <= 31){
		 					return true;
		 					}else
		 					if(a == 8 && g <= 31){
		 					return true;
		 					}else
		 					if(a == 9 && g <= 30){
		 					return true;
		 					}else
		 					if(a == 10 && g <= 31){
		 					return true;
		 					}else
		 					if(a == 11 && g <= 30){
		 					return true;
		 					}else
		 					if(a == 12 && g <= 31){
		 					return true;
		 					}else{
		 					return false;
		 					}
		 			}else{
		 				return false;
		 			}
		 }
 		public void addDate(){

 					if(isValid((gun+1), ay, yil)){
 						gun+= 1;
 					}else if(isValid(1,(ay+1),yil)){
 						ay+=1;
 						gun = 1;
 					}else{
 						ay = 1;
 						gun = 1;
 						yil += 1;
 					}
 		}
 		public void subtractDate(){
 	 		if(ay == 1){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 12;
 	 			gun = 31;
 	 			yil -= 1;
 	 			}
 	 			}
 	 		else if(ay == 2){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 1;
 	 			gun = 31;
 	 			}
 	 			}
 	 		else if(ay == 3 && yil % 4 != 0){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 2;
 	 			gun = 28;
 	 			}
 	 		    }
 	 		else if(ay == 3 && yil % 4 == 0 && yil % 400 != 0){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 2;
 		 		gun = 29;
 	 			}
 	 		 	}
 	 		else if(ay == 4){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 3;
 	 			gun = 31;
 	 			}
 	 		    }
 	 		else if(ay == 5){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 4;
 	 			gun = 30;
 	 			}
 	 			}
 	 		else if(ay == 6){
 	 			if(isValid(gun-1, ay, yil)){
 				gun-= 1;
 	 			}else{
 	 			ay = 5;
 	 			gun = 31;
 	 			}
 	 			}
 	 		else if(ay == 7){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 6;
 	 			gun = 30;
 	 			}
 	 			}
 	 		else if(ay == 8){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 7;
 	 			gun = 31;
 	 			}
 	 			}
 	 		else if(ay == 9){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 8;
 	 			gun = 31;
 	 			}
 	 			}
 	 		else if(ay == 10){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 9;
 	 			gun = 30;
 	 			}
 	 			}
 	 		else if(ay == 11){
 	 			if(isValid(gun-1, ay, yil)){
 	 			gun-= 1;
 	 			}else{
 	 			ay = 10;
 	 			gun = 31;
 	 			}
 	 			}
 	 		else if(ay == 12){
 	 			if(isValid(gun-1, ay, yil)){
 		 		gun-= 1;
 	 			}else{
 		 		ay = 11;
 	 			gun = 30;
 	 			}
 	 			}
 	 }
 	 	public void decrementDay(int n){

 	 		for(int count = 0; count < n; count++){
 	 			subtractDate();
 	 }
 	 }
 	 	public void incrementDay(int n){

 			for(int count = 0; count < n; count++){
 					addDate();
 		}
 	}
 	 	public void decrementMonth(int n){

 		 	for(int count = 0; count < n; count++){
 		 		if(ay == 3 && yil % 4 != 0){
 		 			if(isValid(gun, ay-1, yil)){
 		 			ay-= 1;
 		 			}else{
 		 			ay = 2;
 		 			gun = 28;
 		 			}
 		 			}
 		 		    else if(ay == 3 && yil % 4 == 0 && yil % 400 != 0){
 		 			if(isValid(gun, ay-1, yil)){
 		 			ay-= 1;
 		 			}else{
 		 			ay = 2;
 		 			gun = 29;
 		 			}
 		 			}
 		 		else if(ay == 2 && yil % 4 != 0){
 		 			if(isValid(gun, ay-1, yil)){
 		 			ay-= 1;
 		 			}else{
 		 			ay = 1;
 		 			gun = 30;
 		 			}
 		 			}
 		 			else if(ay == 2 && yil % 4 == 0 && yil % 400 != 0){
 		 			if(isValid(gun, ay-1, yil)){
 		 			ay-= 1;
 		 			}else{
 		 			ay = 1;
 		 			gun = 30;
 		 			}
 		 			}
 		 		else if(ay == 1&& yil % 4 != 0){
 		 			if(isValid(gun, ay-1, yil)){
 		 			ay-= 1;
 		 			}else{
 		 			ay = 12;
 		 			gun = 30;
 		 			yil--;
 		 			}
 		 			}
 		 			else if(ay == 1 && yil % 4 == 0 && yil % 400 != 0){
 		 			if(isValid(gun, ay-1, yil)){
 		 			ay-= 1;
 		 			}else{
 		 			ay = 12;
 		 			gun = 30;
 		 			yil--;
 		 			}
 		 			}
 		 		else{
 		 			for(int count2 = 0; count2 < 30; count2++){
 		 					subtractDate();
 			 	}
 	          }
 		 	}
 	 	  }

 	public void incrementMonth(int n){
 			if(n > 12) {
 				yil++;
 				n = n - 12 ;
     }
 			for(int count = 0; count < n; count++){
 				if(ay == 3 && yil % 4 != 0){
 					if(isValid(gun, ay+1, yil)){
 					ay+= 1;
 					}else{
 					ay = 2;
 					gun = 28;
 					}
 					}
 					else if(ay == 3 && yil % 4 == 0 && yil % 400 != 0){
 					if(isValid(gun	, ay+1, yil)){
 					ay+= 1;
 					}else{
 					ay = 2;
 					gun = 29;
 					}
 					}
 				else if(ay == 2 && yil % 4 != 0){
 					if(isValid(gun, ay+1, yil)){
 					ay+= 1;
 					}else{
 					ay = 1;
 					gun = 30;
 					}
 					}
 					else if(ay == 2 && yil % 4 == 0 && yil % 400 != 0){
 					if(isValid(gun, ay+1, yil)){
 					ay+= 1;
 					}else{
 					ay = 1;
 					gun = 30;
 					}
 					}
 				else if(ay == 1&& yil % 4 != 0){
 					if(isValid(gun, ay+1, yil)){
 					ay+= 1;
 					}else{
 					ay = 12;
 					gun = 30;
 					yil++;
 					}
 					}
 					else if(ay == 1 && yil % 4 == 0 && yil % 400 != 0){
 					if(isValid(gun, ay+1, yil)){
 					ay+= 1;
 					}else{
 					ay = 12;
 					gun = 30;
 					yil++;
 					}
 					}
 				else{
 					for(int count2 = 0; count2 < 30; count2++){
 						addDate();
 					}
 				}
 			}
 	     }
	public void incrementYear(int n){
		 
			for(int count = 0; count < n; count++){
					yil++;
		}
}
 	public void decrementYear(int n){
 			for(int count = 0; count < n; count++){
 			if(ay == 2 && yil-n % 4 != 0){
 					if(isValid(gun, ay, yil-1)){
 					yil-= 1;
 					gun = 29;
 					}else{
 					ay = 2;
 					gun = 28;
 					yil-= 1;
 					}
 					}
 			else if(ay == 2 && yil-n % 4 == 0 && yil % 400 != 0){
 					if(isValid(gun, ay, yil-1)){
 					yil-= 1;
 					}else{
 					ay = 2;
 					gun = 29;
 					yil-= 1;
 					}
 					}
 
 			else{
 					yil--;
 				}
 		}
 	}

 		public boolean isBefore(MyDate anotherDate) {

	 	if(this.getMonth() < anotherDate.getMonth() && this.getYear() < anotherDate.getYear()){
	 	return true;
 }		else{
	 	return false;
 	}	
 		}

 		public boolean isAfter(MyDate anotherDate) {
        
        if(this.getMonth() >= anotherDate.getMonth() && this.getYear() >= anotherDate.getYear()){
         return true;
         }else{
         return false;
         }
 		}

 		public int dayDifference(MyDate anotherDate){
 			return gun;
    	}
	}

