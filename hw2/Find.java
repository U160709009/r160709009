import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;
public class Find {
	public static void main(String[] args) {
		int [][] matrix = readMatrix();
		for (int a = 0; a < 10; a++)  {
			int mert = 0;
			for (int b = 0; b < matrix.length; b++) 
				{
				for (int c = 0; c < matrix.length; c++) 
					{
					if (a == matrix [b][c])
						mert++;
					}
				}
			System.out.println(a + " occurs " + mert + " time(s)");
			}
	}
	private static int[][] readMatrix(){
		int[][] matrix = new int [10][10];
		File file = new File("matrix.txt");
	    try {
	        Scanner sc = new Scanner(file);
	        int i = 0;
	        int j = 0;
	        while (sc.hasNextLine()) {
	            int number = sc.nextInt();
	            matrix[i][j] = number;
	            if (j == 9)
	            	i++;
	            j = (j + 1) % 10;
	            if (i==10)
	            	break;
	        }
	        sc.close();
	    } 
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }		
		return matrix;
	}
	
}
